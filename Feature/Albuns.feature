Feature:Album fonctionalities testing

  @First
  Scenario: Search albuns using a search text
    Given The user is in the home page
    When The user fill the searching box
      |Pedro|
    And The user clicks on the magnifying glass
    Then A list of photos must be displayed
    When The user clicks on the option Pessoas
    And The user clicks on one of the displayed profiles
    And The user clicks on the option Albuns
    Then The user clicks on one of the displayed Albuns
    And The set of photos of the album must be displayed
