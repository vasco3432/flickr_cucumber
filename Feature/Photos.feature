Feature:Photos fonctionalities testing

@First
Scenario: Search photos using a search text
  Given The user is in the home page
  When The user fill the searching box
  |irlanda|
  And The user clicks on the magnifying glass
  Then A list of photos must be displayed

@Second
Scenario: Open one Photo from the list
  Given The user is in the home page
  When The user fill the searching box
    |irlanda|
  And The user clicks on the magnifying glass
  Then A list of photos must be displayed
  When The user click on one of the photos
  Then The photo is maximized
  And A set of details of the photo are displayed

@Third
Scenario: Download a photo
  Given The user is in the home page
  When The user fill the searching box
    |irlanda|
  And The user clicks on the magnifying glass
  Then A list of photos must be displayed
  When The user click on one of the photos
  Then The photo is maximized
  And A set of details of the photo are displayed
  Given One photo is maximized
  When The user clicks on the option download
  And Clicks on one of the possible download sizes
    |irlanda|
  Then The photo must be downloaded