package PageElements;

import PageMethods.HomePageAfterLogin;
import PageMethods.HomePageBeforeLogin;
import PageMethods.LoginPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class TestBase {


    public static WebDriver initialize() {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\chromedriver.exe");
        HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
        chromePrefs.put("download.default_directory", System.getProperty("user.dir"));
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("prefs", chromePrefs);
        WebDriver driver = new ChromeDriver(options);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.get("https://flickr.com");
        return driver;
    }

    public static void goToUserHomePage(WebDriver driver, HomePageBeforeLogin hpBeforeLogin, LoginPage loginPage, HomePageAfterLogin hpAfterLogin) {
        if (driver.getTitle().contains("Encontre sua inspiração. | Flickr")) {
            hpBeforeLogin.goToLoginPage();
            loginPage.login("vasco.gcoutinho@gmail.com", "istoeumexercicio");
        } else if (driver.getTitle().contains("Flickr Login")) {
            loginPage.login("vasco.gcoutinho@gmail.com", "istoeumexercicio");
        } else {
            hpAfterLogin.goToHomePage();
        }
    }


}


