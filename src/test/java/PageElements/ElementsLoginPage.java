package PageElements;

import org.openqa.selenium.By;

public class ElementsLoginPage {
	
	public By user=By.id("login-email");
	public By pwd=By.id("login-password");
	public By next= By.xpath("//*[contains(text(),'Pr�xima')]");
	public By submit= By.xpath("//*[contains(text(),'Fazer login')]");
	
}
