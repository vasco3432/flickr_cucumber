package PageElements;

import org.openqa.selenium.By;

public class ElementsHomePageAfterLogin {
//	Test Set 1 and 2
	public By goToHomePage=By.xpath("//a[@class='main-logo']");
	public By searchInput=By.id("search-field");
	public By inputClick = By.xpath("//input[@type='submit']");
	public By closeCookies=By.xpath("//div[@class='button-container']");
	
//	Test Set 1
	public By photosResultSet= By.xpath("//div[@class='view photo-list-photo-view awake']");
	public By downloadButton= By.xpath("//a[@title='Baixar esta foto']");
	public By goBackDownload= By.xpath("/html/body/div[7]");
	public By sizePhoto= By.xpath("//ul[@class='sizes']/li");

//	Asserts Test1
	public By colors=By.xpath("//ul[@class='color-list']");
	public By advanced=By.xpath("//div[@class='advanced-toggle']");

//	Asserts Test2
	public By camera=By.xpath("//div[@class='exif-column-1']");
	public By views=By.xpath("//div[@class='right-stats-details-container']");
	public By info=By.xpath("//h3[contains(text(),'Informa��es adicionais')]");
	
//	Test Set 2
	public By pessoasResult= By.xpath("/html/body/div[1]/div/div[2]/div/div/div/ul[1]/li[2]/a/div");
	public By pessoasList= By.xpath("//div[@class='result-card linked reboot-restyle']");
	public By albunsResult= By.xpath("//span[contains(text(),'�lbuns')]");
	public By albunsList= By.xpath("//div[@class='view photo-list-view']/div");
	public By dismiss=By.xpath("/html/body/div[1]/div/div[7]/div/div/div/div/div/div[2]/a");

//	Asserts Test1
	public By albumPhotos=By.xpath("//div[@class='photo-list-photo-interaction']");
}


