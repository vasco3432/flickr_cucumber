package TestRunner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features="Feature",
        glue={"StepDefinitions"},
        plugin = {"pretty","html:TestOutput/htmlReport", "json:TestOutput/jsonReport/cucumber.json"},
        dryRun=false)

public class Runner {
}