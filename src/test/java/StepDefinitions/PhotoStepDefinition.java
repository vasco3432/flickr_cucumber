package StepDefinitions;


import PageElements.TestBase;
import PageMethods.HomePageAfterLogin;
import PageMethods.HomePageBeforeLogin;
import PageMethods.LoginPage;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class PhotoStepDefinition {


   HomePageBeforeLogin hpBeforeLogin=null;
   LoginPage loginPage=null;
   HomePageAfterLogin hpAfterLogin=null;
   WebDriver driver;

   @Before
      public void setUp(){
       this.driver=TestBase.initialize();
       this.hpBeforeLogin = new HomePageBeforeLogin(driver);
       this.loginPage = new LoginPage(driver);
       this.hpAfterLogin = new HomePageAfterLogin(driver);
      TestBase.goToUserHomePage(driver, hpBeforeLogin, loginPage, hpAfterLogin);
      WebDriverWait wait=new WebDriverWait(driver, 10);
      wait.until(ExpectedConditions.titleIs("P�gina inicial | Flickr"));

   }

    @After
    public void tearDown(){driver.close();}

    @Given("The user is in the home page")
    public void theUserIsInTheHomePage() {
        Assert.assertTrue(hpAfterLogin.checkDriverPosition());
    }

    @When("The user fill the searching box")
    public void theUserFillTheSearchingBox(String searchText) {
        hpAfterLogin.searchTextInput(searchText);
    }

    @And("The user clicks on the magnifying glass")
    public void theUserClicksOnTheMagnifyingGlass() throws InterruptedException {
       hpAfterLogin.magnifyingGlass();

    }

    @Then("A list of photos must be displayed")
    public void aListOfPhotosMustBeDisplayed() {
       Assert.assertTrue(hpAfterLogin.assertSearchPhoto());
    }

    @Given("The list has at least one photo")
    public void theListHasAtLeastOnePhoto() {
        Assert.assertTrue(hpAfterLogin.assertSearchPhoto());
    }

    @When("The user click on one of the photos")
    public void theUserClickOnOneOfThePhotos() {
       hpAfterLogin.openPhoto();
    }

    @Then("The photo is maximized")
    public void thePhotoIsMaximized() {

    }

    @And("A set of details of the photo are displayed")
    public void aSetOfDetailsOfThePhotoAreDisplayed() {
        hpAfterLogin.assertOpenPhoto();
    }

    @Given("One photo is maximized")
    public void onePhotoIsMaximized() {
        hpAfterLogin.assertOpenPhoto();
    }

    @When("The user clicks on the option download")
    public void theUserClicksOnTheOptionDownload() {
       hpAfterLogin.closeCookiesPolicy();
       hpAfterLogin.clickDownloadPhoto();
    }

    @And("Clicks on one of the possible download sizes")
    public void clicksOnOneOfThePossibleDownloadSizes(String searchText) throws InterruptedException {
       hpAfterLogin.downloadPhoto(searchText);
    }

    @Then("The photo must be downloaded")
    public void thePhotoMustBeDownloaded() {
       hpAfterLogin.assertDownload();
    }
}
