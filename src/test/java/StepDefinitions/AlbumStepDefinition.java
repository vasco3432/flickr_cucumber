package StepDefinitions;

import PageElements.TestBase;
import PageMethods.HomePageAfterLogin;
import PageMethods.HomePageBeforeLogin;
import PageMethods.LoginPage;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;



public class AlbumStepDefinition {


    HomePageBeforeLogin hpBeforeLogin=null;
    LoginPage loginPage=null;
    HomePageAfterLogin hpAfterLogin=null;
    WebDriver driver;

    @Before
    public void setUp(){
        this.driver= TestBase.initialize();
        this.hpBeforeLogin = new HomePageBeforeLogin(driver);
        this.loginPage = new LoginPage(driver);
        this.hpAfterLogin = new HomePageAfterLogin(driver);
        TestBase.goToUserHomePage(driver, hpBeforeLogin, loginPage, hpAfterLogin);
        WebDriverWait wait=new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.titleIs("P�gina inicial | Flickr"));

    }

    @After
    public void tearDown(){driver.close();}

    @When("The user clicks on the option Pessoas")
    public void theUserClicksOnTheOptionPessoas(){
        hpAfterLogin.goToPeopleSearch();
    }

    @And("The user clicks on one of the displayed profiles")
    public void theUserClicksOnOneOfTheDisplayedProfiles() {
        hpAfterLogin.openPerson();
    }

    @And("The user clicks on the option Albuns")
    public void theUserClicksOnTheOptionAlbuns() {
        hpAfterLogin.goToAlbunsSearch();
    }

    @Then("The user clicks on one of the displayed Albuns")
    public void theUserClicksOnOneOfTheDisplayedAlbuns() {
        hpAfterLogin.openAlbum();
    }

    @And("The set of photos of the album must be displayed")
    public void theSetOfPhotosOfTheAlbumMustBeDisplayed() {
       Assert.assertTrue( hpAfterLogin.assertAlbumOpened());
    }
}
