package PageMethods;


import org.openqa.selenium.WebDriver;

import PageElements.ElementsLoginPage;

public class LoginPage {
	
	ElementsLoginPage elements= new ElementsLoginPage();
	WebDriver driver;

	public LoginPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public void login(String username, String password){
		driver.findElement(elements.user).clear();
		driver.findElement(elements.user).sendKeys(username);
		driver.findElement(elements.next).click();
		driver.findElement(elements.pwd).clear();
		driver.findElement(elements.pwd).sendKeys(password);
		driver.findElement(elements.submit).click();}
}
