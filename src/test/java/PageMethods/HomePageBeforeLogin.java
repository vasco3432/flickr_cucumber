package PageMethods;


import org.openqa.selenium.WebDriver;


import PageElements.ElementsHomePageBeforeLogin;

public class HomePageBeforeLogin {
	
	ElementsHomePageBeforeLogin elements=new ElementsHomePageBeforeLogin();
	WebDriver driver=null;

	public HomePageBeforeLogin(WebDriver driver) {
		this.driver = driver;
	}
	
	public void goToLoginPage(){
		driver.findElement(elements.login).click();
		}
	
	
}
