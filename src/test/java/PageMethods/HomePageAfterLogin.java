package PageMethods;

import java.io.File;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.*;

import PageElements.ElementsHomePageAfterLogin;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class HomePageAfterLogin {

	ElementsHomePageAfterLogin elements = new ElementsHomePageAfterLogin();
	WebDriver driver;
	String currentDirectory = System.getProperty("user.dir");

	public HomePageAfterLogin(WebDriver driver) {
		this.driver = driver;
	}

	public void goToHomePage() {
		driver.findElement(elements.goToHomePage).click();
	}

	public void searchTextInput(String searchText) {
		driver.findElement(elements.searchInput).sendKeys(searchText);
	}

	public void magnifyingGlass() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(elements.inputClick));
		driver.findElement(elements.inputClick).click();
	}

	public void closeCookiesPolicy() {
		driver.findElement(elements.closeCookies).click();
	}

	public void openPhoto() {
		Random rand = new Random();
		List<WebElement> listPhotos = driver.findElements(elements.photosResultSet);
		listPhotos.get(rand.nextInt(listPhotos.size())).click();
	}


	public void clickDownloadPhoto() {
		driver.findElement(elements.downloadButton).click();
	}

	public void downloadPhoto(String searchText) throws InterruptedException {
		try {
			Random rand = new Random();
			List<WebElement> listSizes = driver.findElements(elements.sizePhoto);
			if (listSizes.size() <= 1) {
				this.retryTestSetOne(searchText);
				downloadPhoto(searchText);
				return;
			}
			WebElement element = listSizes.get(rand.nextInt(listSizes.size() - 1));
			element.click();

		} catch (ElementClickInterceptedException e) {
			int a = driver.manage().window().getSize().getWidth() / 5;
			((JavascriptExecutor) driver).executeScript("window.scrollBy(0," + String.valueOf(a) + ")");
			this.downloadPhoto("Eti�pia");
		}
	}

	public void goToPeopleSearch() {
		driver.findElement(elements.pessoasResult).click();
	}


	public void openPerson() {
		try {
			Random rand = new Random();
			List<WebElement> listPeople = driver.findElements(elements.pessoasList);
			listPeople.get(rand.nextInt(listPeople.size())).click();
		} catch (ElementClickInterceptedException e) {
			openPerson();

		}
	}

	public void goToAlbunsSearch() {
		driver.findElement(elements.albunsResult).click();
	}

	public boolean openAlbum() {
		Random rand = new Random();
		List<WebElement> listAlbuns = driver.findElements(elements.albunsList);
		if (listAlbuns.size() == 0)
			return false;
		driver.findElement(elements.dismiss).click();
		WebElement element = listAlbuns.get(rand.nextInt(listAlbuns.size()));
		element.click();
		return true;
	}

	public void retryTestSetOne(String searchText) throws InterruptedException {
		driver.findElement(elements.goBackDownload).click();
		this.goToHomePage();
		this.searchTextInput(searchText);
		this.magnifyingGlass();
		this.openPhoto();
	}

	public boolean assertDownload() {
		driver.findElement(elements.goBackDownload).click();
		String toSplit = driver.findElement(elements.downloadButton).getAttribute("href");
		String[] arrString = toSplit.split("/");
		String downloadTitle = arrString[5];
		File folder = new File(currentDirectory);
		File[] listOfFiles = folder.listFiles();
		for (File listOfFile : listOfFiles) {
			if (listOfFile.getName().contains(downloadTitle))
				return true;
		}
		return false;
	}

	public boolean assertSearchPhoto() {
		if (!driver.findElements(elements.photosResultSet).isEmpty() && driver.findElement(elements.colors).isDisplayed() && driver.findElement(elements.advanced).isDisplayed())
			return true;
		else return false;
	}

	public boolean assertOpenPhoto() {
		if (driver.findElement(elements.camera).isDisplayed() && driver.findElement(elements.views).isDisplayed() && driver.findElement(elements.info).isDisplayed())
			return true;
		else return false;
	}

	public boolean checkDriverPosition() {
		return driver.getTitle().equals("P�gina inicial | Flickr");
	}

	public boolean assertAlbumOpened(){
		return !driver.findElements(elements.albumPhotos).isEmpty();
	}
}


